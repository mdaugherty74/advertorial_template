var gulp = require('gulp'),
    autoprefixer = require('gulp-autoprefixer'),
    sourcemaps = require('gulp-sourcemaps'),
    sass = require('gulp-sass'),
    browserSync = require('browser-sync'),
    del = require('del')

gulp.task('styles', function() {
    return gulp
        .src(`./src/sass/**/*.scss`)
        .pipe(
            autoprefixer({
                browsers: ['last 2 versions'],
                cascade: false,
            })
        )
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.write('/maps'))
        .pipe(gulp.dest(`./src/css`))
        .pipe(
            browserSync.reload({
                stream: true,
            })
        )
})

gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: './src',
        },
    })
})

gulp.task('watch', function() {
    gulp.watch('./src/sass/*', ['styles'])
    gulp.watch(['./src/js/**/*.js', './src/*.html']).on('change', browserSync.reload)
})

gulp.task('default', ['styles', 'browser-sync'], function() {
    gulp.start('watch')
})

gulp.task('clean', function() {
    return del.sync('dist')
})
gulp.task('removeSassDir', function() {
    setTimeout(() => {
        return del.sync('dist/sass')
    }, 300)
})

gulp.task('copy', function() {
    return gulp.src(['src/**/*', '!src/sass/*'], { base: 'src' }).pipe(gulp.dest('dist'))
})

gulp.task('build', ['clean', 'copy', 'removeSassDir'])
